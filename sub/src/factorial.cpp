
#include <factorial.hpp>

namespace {
    std::uint64_t fact (std::uint64_t accum, std::uint64_t value) {
        if (value == 0) {
            return accum ;
        }
        return fact (value * accum, value - 1) ;
    }
}

std::uint64_t factorial (std::uint64_t value) {
    return fact (1, value) ;
}
