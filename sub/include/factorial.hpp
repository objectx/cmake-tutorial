
#pragma once
#ifndef factorial_hpp__01f54d012b584b5696398d10e3b9df81
#define factorial_hpp__01f54d012b584b5696398d10e3b9df81 1

#include <cstdint>

//! Compute factorial of @a value
extern std::uint64_t factorial (std::uint64_t value) ;

#endif /* end of include guard: factorial_hpp__01f54d012b584b5696398d10e3b9df81 */
