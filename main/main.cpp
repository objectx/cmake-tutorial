
#include "config.hpp"

#include <iostream>
#include <factorial.hpp>

int main (int argc, char **argv) {
    std::cout << "Hello World." << std::endl ;
    std::cout << "Factorial (5) = " << factorial (5) << std::endl ;
    return 0 ;
}
